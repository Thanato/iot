#include <rgb_lcd.h> 
#define CPT_T A1
#define PTM A2
#define RELAY 2

rgb_lcd lcd;

struct Color {
  int R = 0;
  int G = 0;
  int B = 0;  
};

struct Screen {
  char line_1[15];
  char line_2[15];
  Color color;
};

struct Potentiometer {
  float value = 0.0;
};
Potentiometer potentiometer;

struct Cpt_temperature {
  float degree = 0;
};

struct Relay {
  byte value;
};

Screen screen;
Cpt_temperature cpt_temperature;
Relay relay;


void setup() {
  pinMode(CPT_T, INPUT);
  pinMode(PTM, INPUT);
  pinMode(RELAY, OUTPUT);
  
  screen.color.R = 0;
  screen.color.G = 255;
  screen.color.B = 0;

  relay.value = 0;

  lcd.begin(16, 2);
  lcd.setRGB(screen.color.R, screen.color.G, screen.color.B);

  delay(1000);
}

void loop() {
  cpt_temperature.degree = analogRead(CPT_T) * 50 / 1024.0;
  dtostrf(cpt_temperature.degree, 3, 2, screen.line_2);

  potentiometer.value = map(analogRead(PTM), 0, 1023, -30, 30);  
  dtostrf(potentiometer.value, 1, 1, screen.line_1);

  lcd.setCursor(5, 0);
  lcd.print(screen.line_1);
  lcd.setCursor(5, 1);
  lcd.print(screen.line_2);

  screen.color.R = 0;
  screen.color.G = 255;
  relay.value = LOW;
  if (potentiometer.value < cpt_temperature.degree)
  {
    screen.color.R = 255;
    screen.color.G = 0;
    relay.value = HIGH;
  }
  
  digitalWrite(RELAY, relay.value);
  lcd.setRGB(screen.color.R, screen.color.G, screen.color.B);
  delay(100);
}
