#include <Servo.h>

#define LED 6
#define RELAY 2
#define MOTOR A1

struct Led {
  int intensity;
};
struct Led checkLedValue(int value);

struct Motor {
  int angle;
};
struct Motor checkMotorValue(int value);

struct Relay {
  byte value;
};
struct Relay checkRelayValue(byte value);

String arr;

struct Param {
  Led led;
  Motor motor;
  Relay relay;
};

Param params;
Servo servo;

void setup() {
  // put your setup code here, to run once:
  pinMode(LED, OUTPUT);
  pinMode(RELAY, OUTPUT);

  servo.attach(MOTOR);
  
  Serial.begin(9600);
}

void loop() {
  // put your main code here, to run repeatedly:
  if (Serial.available() > 0 ) {
    arr = Serial.readString(/*'\n', arr, sizeof(arr) / sizeof(char)*/);

    params = parseParam(arr);
    
    analogWrite(LED, params.led.intensity);
    //analogWrite(MOTOR, params.motor.angle);
    digitalWrite(RELAY, params.relay.value);

    servo.write(params.motor.angle);
    
    //memset(arr, 0, sizeof(arr));
  }
  
  delay(100);
}

struct Led checkLedValue(int value) {
  Led led;
  led.intensity = value;
  
  if (value > 255) led.intensity = 255;
  if (value < 0) led.intensity = 0;

  return led;
}

struct Motor checkMotorValue(int value) {
  Motor motor;
  motor.angle = value;
  
  if (value > 120) motor.angle = 120;
  if (value < 5) motor.angle = 5;

  return motor;
}

struct Relay checkRelayValue(byte value) {
  Relay relay;
  relay.value = value;
  
  if (value > 1) relay.value = 1;
  if (value < 0) relay.value = 0;

  return relay;
}

struct Param parseParam(String p) {
  String params = p;
  char startedCharacter = '*';
  char endedCharacter = ';';
  char separatorCharacter = ':';
  Param parsedParams;
  
  int indexOfStartedCharacter = -1;
  int indexOfEndedCharacter = -1;

  do {
    indexOfStartedCharacter = params.indexOf(startedCharacter);
    indexOfEndedCharacter = params.indexOf(endedCharacter);
    
    if (indexOfStartedCharacter != -1 && indexOfEndedCharacter != -1 && indexOfStartedCharacter < indexOfEndedCharacter) {
      String param = params.substring(indexOfStartedCharacter + 1, indexOfEndedCharacter);
      params.remove(indexOfStartedCharacter, indexOfEndedCharacter + 1);

      // TODO: Traitement du param
      int indexOfSeparator = param.indexOf(separatorCharacter);
      
      if (indexOfSeparator != -1) {
        String named = param.substring(0, indexOfSeparator);
        named.toUpperCase();
        int value = param.substring(indexOfSeparator + 1).toInt();

        if (named == "LED") parsedParams.led = checkLedValue(value);
        if (named == "MOTOR") parsedParams.motor = checkMotorValue(value);
        if (named == "RELAIS") parsedParams.relay = checkRelayValue(value);
      }
    }

  } while (indexOfStartedCharacter != -1 && indexOfEndedCharacter != -1);

  return parsedParams;
}
