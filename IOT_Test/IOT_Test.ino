#define LED 6
#define BTN 7

struct Led {
  int state = 0;
};

struct Button {
  int state = 0;
  int oldState = 0;
};

struct Button button;
struct Led led;

void setup() {
  pinMode(BTN, INPUT);
  pinMode(LED, OUTPUT);
}

void loop() {
  // Récupération des états
  button.state = digitalRead(BTN);

  // Détection
  if (button.state == HIGH && button.state != button.oldState) {
    reverseBool(led.state);
  }

  // Mise à jour des états
  if(hasChangedStatus(button)) {
    button.oldState = button.state;
  }

  digitalWrite(LED, led.state);
}

void reverseBool(int& stateToChange) {
  if (stateToChange == 0)
    stateToChange = 1;
  else    
    stateToChange = 0;
}

int hasChangedStatus(Button& pButton)
{
  return pButton.state != pButton.oldState;
}
