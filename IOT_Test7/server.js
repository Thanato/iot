const server = require('http').createServer();
const io = require('socket.io')(server);

io.on('connection', socket => {
    console.log("connection")
    io.to(socket.id).emit('connected');
});

server.listen(8082,function(){ // Listens to port 8082
    console.log('Listening on ' + server.address().port);
});