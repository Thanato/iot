const { SerialPort, ReadlineParser } = require('serialport')

const sp = new SerialPort({
    path: 'COM5',
    baudRate: 9600,
})

const parser = new ReadlineParser();

sp.pipe(parser)
sp.on("open", function () {
    sp.write("data	vers	arduino\n");
    console.log('open	/dev/tty…');
    parser.on('data', (data) => {
        console.log('	data	:	' + data);
    });
});