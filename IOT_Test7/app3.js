var five = require("johnny-five");
const sleep = require("johnny-five/lib/sleep");
var mqtt = require('mqtt');

var mqtt_client = mqtt.connect('mqtts://mqtt3.thingspeak.com:8883', {
    clientId: 'EQUfKgs3IgwaJDAhLjsrECM',
    username: 'EQUfKgs3IgwaJDAhLjsrECM',
    password: 'Sji2/GEOCzgSQE2oRy5SAdfp',
});

var mqtt_channel = 1747138;

var mqtt_connected = false;

mqtt_client.on('connect', function () {
    console.log('Connected on MQTT server');
    mqtt_connected = true;
});

var board = new five.Board({repl:false});

board.on("ready", function() {

    console.log('Connected to Arduino');

    var ligth_sensor = new five.Light({
        pin: "A0",
        controller: "TSL2561",
        freq: 1000,
    });

    var temperature_senor = new five.Thermometer({
        controller: "GROVE",
        pin: "A1",
        freq: 1000,
      });

    var sound_sensor = new five.Sensor({
        pin: "A2",
        freq: 1000,
    });
    
    ligth_sensor.on('change', function () {
        perform('field1', this.level);
    });

    temperature_senor.on('change', function () {
        perform('field2', this.celsius);
    })

    sound_sensor.on('change', function () {
        perform('field3', this.value);
    })
});

var perform = function (field, data) {
    if (mqtt_connected) {
        mqtt_client.publish(`channels/${mqtt_channel}/publish/fields/${field}`, data.toString(), {
            retain: true,
            qos: 0,
        }, function () {
            console.log(`${data.toString()} is send to ${field} field`);
        })
    } else {
        console.log('The MQTT server is disconnected')
    }
}