var five = require("johnny-five");
var board = new five.Board({repl:false});

board.on("ready", function() {

    var lcd = new five.LCD({
        controller: "JHD1313M1"
    });

    lcd.bgColor("yellow");
    lcd.cursor(0,0);

    // Plug the Rotary Angle sensor module
    // into the Grove Shield's A0 jack
    var rotary = new five.Sensor("A0");

    // Plug the LED module into the
    // Grove Shield's D6 jack. See
    // grove-led for more information.
    var led = new five.Led(6);

    // Set scaling of the Rotary angle
    // sensor's output to 0-255 (8-bit)
    // range. Set the LED's brightness
    // based on the value of the sensor.
    rotary.scale(0, 255).on("change", function() {
        led.brightness(this.value);
        lcd.clear().print(this.value);
    });
});