#define PTM A2

struct Potentiometer {
  float value;  
};

int show;

Potentiometer potentiometer;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  
  while (Serial.read() != 'A') {}
}

void loop() {
  // put your main code here, to run repeatedly:
  if (Serial.read() == 'd') {
    show = 1; 
  }

  if (show) {
    potentiometer.value = analogRead(PTM);

    Serial.println(potentiometer.value);    
  }
  
  delay(100);
}
