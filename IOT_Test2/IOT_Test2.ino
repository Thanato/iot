#include <rgb_lcd.h> 
#define CPT_T A1
#define CPT_L A0

rgb_lcd lcd;

struct Color {
  int R = 0;
  int G = 0;
  int B = 0;  
};

struct Screen {
  char line_1[15];
  char line_2[15];
  Color color;
};

struct Cpt_luminosity {
  float luminosity = 0;
};


struct Cpt_temperature {
  float degree = 0;
};

Screen screen;
Cpt_luminosity cpt_luminosity;
Cpt_temperature cpt_temperature;


void setup() {
  pinMode(CPT_T, INPUT);
  pinMode(CPT_L, INPUT);
  
  screen.color.R = 255;
  
  // put your setup code here, to run once:
  lcd.begin(16, 2);
  lcd.setRGB(screen.color.R, screen.color.G, screen.color.B);

  delay(1000);
}

void loop() {
  cpt_temperature.degree = analogRead(CPT_T) * 50 / 1024.0;
  cpt_luminosity.luminosity = analogRead(CPT_L) * 50 / 1024.0;
  dtostrf(cpt_temperature.degree, 3, 2, screen.line_1);
  dtostrf(cpt_luminosity.luminosity, 3, 2, screen.line_2);
  // put your main code here, to run repeatedly:
  // set the cursor to column 0, line 1
  // (note: line 1 is the second row, since counting begins with 0):
  lcd.setCursor(0, 0);
  // print the number of seconds since reset:
  lcd.print(screen.line_1);

  // (note: line 1 is the second row, since counting begins with 0):
  lcd.setCursor(0, 1);
  // print the number of seconds since reset:
  lcd.print(screen.line_2);
  
  delay(100);
}
