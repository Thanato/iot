#include <Servo.h>

#define LED 6
#define PTM A2
#define MOTOR 8

Servo servo;

struct Potentiometer {
  float value = 0.0;
  float valueto120 = 0.0;
  float valueto255 = 0.0;
};

Potentiometer potentiometer;

void setup() {
  // put your setup code here, to run once:
  pinMode(PTM, INPUT);
  pinMode(LED, OUTPUT);
  
  servo.attach(MOTOR);
}

void loop() {
  // put your main code here, to run repeatedly:
  potentiometer.value = analogRead(PTM);
  potentiometer.valueto120 = map(potentiometer.value, 0, 1023, 0, 120);
  potentiometer.valueto255 = potentiometer.value / 4;
  
  servo.write(potentiometer.valueto120);
  analogWrite(LED, potentiometer.valueto255);
}
